package ru.tsc.fuksina.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

    public AccessDeniedException(@NotNull final Throwable cause) {
        super(cause);
    }

}
