package ru.tsc.fuksina.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
