package ru.tsc.fuksina.tm.exception.entity;

public class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }
}
