package ru.tsc.fuksina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataBase64LoadRequest;
import ru.tsc.fuksina.tm.enumerated.Role;

@Component
public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base64 file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD DATA FROM BASE64 FILE]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

}
