package ru.tsc.fuksina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.service.*;
import ru.tsc.fuksina.tm.command.AbstractCommand;
import ru.tsc.fuksina.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.fuksina.tm.exception.system.CommandNotSupportedException;
import ru.tsc.fuksina.tm.util.SystemUtil;
import ru.tsc.fuksina.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    private void prepareStartup() {
        initPID();
        initCommands(abstractCommands);
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        fileScanner.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initCommands(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
