package ru.tsc.fuksina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.ServerVersionRequest;

@Component
public final class VersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Display application version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
