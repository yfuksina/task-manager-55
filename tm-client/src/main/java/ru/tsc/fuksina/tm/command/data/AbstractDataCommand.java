package ru.tsc.fuksina.tm.command.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.fuksina.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.fuksina.tm.command.AbstractCommand;


@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {


    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @Override
    public @Nullable String getArgument() {
        return null;
    }

}
