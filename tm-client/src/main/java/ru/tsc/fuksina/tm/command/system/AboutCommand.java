package ru.tsc.fuksina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.ServerAboutRequest;

@Component
public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        System.out.println("Author: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());
    }

}
